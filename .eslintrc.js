module.exports = {
  root: true,
  parser: '@typescript-eslint/parser',
  plugins: [
    '@typescript-eslint',
  ],
  extends: [
    'airbnb-typescript',
    'plugin:prettier/recommended',
  ],
  rules: {
    'no-param-reassign': 'off',
    'react/jsx-props-no-spreading': 'off',
    '@typescript-eslint/no-unused-vars': 'off',
    'import/no-unresolved': 'warn',
    "import/no-dynamic-require": "warn",
    "import/no-unresolved": "warn",
    "@typescript-eslint/no-throw-literal": "warn",
    "jsx-a11y/anchor-is-valid": "warn",
    "jsx-a11y/label-has-associated-control": "warn",
  },
  parserOptions: {
    project: './tsconfig.json',
  },
  env: {
    browser: true,
    node: true,
    mocha: true,
  },
  overrides: [
    {
      files: ["src/**/*.tsx", "src/**/*.ts"],
      excludedFiles: ["src/utils/logger.utils.ts"],
      rules: {
        "no-console": "error",
      },
    },
  ],
};