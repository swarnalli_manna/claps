const userRouter = require("../user.handler");
const rewardRouter = require("../reward.handler")
const rewardNominatorRouter = require("../rewardNominator.handler")
const rewardCommentRouter = require("../rewardComment.handler")

module.exports = (app) => {
  app.use(userRouter);
  app.use(rewardRouter);
  app.use(rewardNominatorRouter);
  app.use(rewardCommentRouter);
};
