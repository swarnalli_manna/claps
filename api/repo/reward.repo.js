const reward = require("../models").rewards;
const rewardComment = require("../models").rewards_comments;

exports.postReward = async (rewardData) => {
    const result = await reward.create(rewardData);
    return result
}

exports.findRewards = async () => {
  const result = await reward.findAll({
    raw: true,
  });
  return result;
};

exports.findRewardClaps = async (id) => {
  const result = await reward.findOne({
    where: {
      id,
    },
    attributes: ["id","name","claps"],

    raw: true,
  });
  return result;
};

exports.patchReward = async (updateData, rewardId) => {
  const result = await reward.update(updateData, {
    where: {
      id: rewardId,
    }
  });
  return result;
};

exports.findRewardComments = async (id) => {
  const result = await rewardComment.findOne({
    where: {
      id,
    },
    raw: true,
  });
  return result;
};