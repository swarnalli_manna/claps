const RewardComments = require("../models").rewards_comments;

exports.postRewardComments = async (rewardData) => {
    const result = await RewardComments.create(rewardData);
    return result
}