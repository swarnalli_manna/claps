const { default: config } = require("../config");
const paginationValidator = require("../validators/pageginate.validator");

exports.paginate = (query, { pageNumber, pageSize }) => {
  const offset = (pageNumber - 1) * pageSize;
  const limit = pageSize;
  return { ...query, offset, limit };
};
exports.applyPaginationFilter = async (req) => {
  const pageNumber = req.query.pageNumber || config.paginate.pageNumber;
  const pageSize = req.query.pageSize || config.paginate.pageSize;
  const validateReq = {
    pageNumber,
    pageSize,
  };
  const errors = await paginationValidator.validatePagination(validateReq);

  if (errors.length) {
    return errors;
  }
  return { pageNumber, pageSize };
};
