export default {
  authority: process.env.REACT_APP_SSO_authority,
  client_id: process.env.REACT_APP_SSO_client_id,
  redirect_uri: `${window.location.origin}/location`,
  response_type: process.env.REACT_APP_SSO_response_type,
  scope: process.env.REACT_APP_SSO_scope,
  post_logout_redirect_uri: window.location.origin,
};
