/* eslint-disable import/prefer-default-export */
import styled from "styled-components";

export const DashboardWrapper = styled.div`
  .bgcolor {
    background: linear-gradient(
      140deg,
      #be10e1 0%,
      #bc70a4 50%,
      #d641bd 75%
    ) !important;
    padding: 10px;
  }
  .bgcolor h1 {
    color: #fff;
    font-size: 35px;
  }
  .listings p {
    font-size: 28px;
  }
  .claps-logo {
    background: url(../images/logo.png) no-repeat;
    background-size: contain;
    width: 20rem;
    min-height: 5.5625rem;
    position: relative;
    color: #222328;
  }
  .claps-logo:before {
    position: absolute;
    width: 0.0625rem;
    height: 5.5625rem;
    background-color: #e3e3e3;
    left: 6.375rem;
    top: 0;
    content: "";
  }
  .claps-logo:after {
    content: "Claps";
    font-size: 1.375rem;
    font-weight: 600;
    position: absolute;
    left: 6.875rem;
    top: 1.8125rem;
  }
  .r1,
  .r2,
  .r3,
  .r4 {
    border: 2px solid #e3e3e3;
    height: 200px;
    padding: 10px;
  }
  a {
    color: #000;
    text-decoration: none;
  }
  .card {
    background-color: #f9f8f9;
  }
  .createbutton {
    margin-block: auto;
  }
  .createbutton button {
    background-color: transparent;
    border: 1px solid #fff;
  }
  .createbutton button a {
    color: #fff;
  }
  .card .card-img-top {
    width: 180px;
    border: 8px solid #c9b3dd6b;
    border-radius: 50%;
    margin-left: 10px;
  }
  .icons img {
    width: 35px;
    cursor: pointer;
  }
  .empcontent p,
  .heading,
  .designation {
    font-family: "Roboto", sans-serif;
  }
  .heading {
    font-size: 22px;
    text-align: center;
    @media only screen and (min-width: 1200px) {
      font-size: 26px;
    }
  }
  .designation {
    font-size: 18px;
    color: #7005cf;
    text-align: center;
    @media only screen and (min-width: 1200px) {
      font-size: 22px;
    }
  }
  .social p {
    font-family: "Roboto", sans-serif;
    font-size: 16px;
    color: #f8914c;
  }
  #fixedbutton {
    width: 58px;
    height: 55px;
    position: fixed;
    bottom: 10px;
    right: 5px;
    z-index: 1000;
  }
`;
