/* eslint-disable jsx-a11y/alt-text */
/* eslint-disable jsx-a11y/anchor-is-valid */
/* eslint-disable jsx-a11y/label-has-associated-control */
/* eslint-disable jsx-a11y/anchor-has-content */
import React from "react";
import Button from "@material-ui/core/Button";
import { Link } from "react-router-dom";
import { CreateRewardWrapper } from "./styled";

const CreateReward = () => {
  return (
    <CreateRewardWrapper>
      <header className="aui-main-header aui-pri-header">
        <a href="www.heart.org" className="aui-skip-content">
          Skip to main content
        </a>
        <nav className="navbar navbar-expand-lg justify-content-between aui-header-content mx-auto">
          <a href="/" className="claps-logo" aria-label="Claps Logo" />
          <button
            className="navbar-toggler ml-2 px-0"
            type="button"
            data-toggle="collapse"
            data-target="#toggleNav"
            aria-controls="toggleNav"
            aria-expanded="false"
            aria-label="Toggle navigation"
          >
            <i className="aha-icon-hamburger" />
          </button>
          <div
            className="justify-content-lg-end collapse navbar-collapse aui-pri-nav"
            id="toggleNav"
          >
            <ul className="navbar-nav mx-lg-3 flex-lg-row flex-column">
              <li className="d-flex nav-item px-lg-3">
                <button type="button" className="btn btn-text nav-link">
                  <a href="/">Home</a>
                </button>
              </li>
              <li className="d-flex nav-item px-lg-3">
                <button type="button" className="btn btn-text nav-link">
                  <a href="/rewards">Rewards</a>
                </button>
              </li>
              <li className="d-flex nav-item dropdown px-lg-3 flex-column">
                <button
                  type="button"
                  className="btn btn-text dropdown-toggle nav-link"
                  id="navDropdown1"
                  data-toggle="dropdown"
                  aria-haspopup="true"
                  aria-expanded="false"
                >
                  My Account
                </button>
                <div
                  className="dropdown-menu p-lg-0 aui-header-dropdown"
                  aria-labelledby="navDropdown1"
                  role="menu"
                >
                  <ul>
                    <li className="">
                      <a className="dropdown-item py-2" href="www.heart.org">
                        Profile
                      </a>
                    </li>
                    <li>
                      <a
                        className="dropdown-item py-2"
                        data-toggle="modal"
                        data-target="#modalTemplate3"
                      >
                        Create Award
                      </a>
                    </li>
                    <li>
                      <a className="dropdown-item py-2" href="www.heart.org">
                        Logout
                      </a>
                    </li>
                  </ul>
                </div>
              </li>
            </ul>
          </div>
        </nav>
      </header>
      <div className="d-flex flex-column flex-grow-1">
        <div className="d-lg-flex container-fluid">
          <div className="aui-sidenav">
            <div className="navbar-expand-lg overflow-hidden">
              <button
                className="navbar-toggler float-right m-3"
                type="button"
                data-toggle="collapse"
                data-target="#sideNavbar"
                aria-controls="sideNavbar"
                aria-expanded="false"
                aria-label="Toggle navigation"
              >
                <i className="aha-icon-hamburger-round" />
              </button>
              <div className="text-center m-3">
                <h4 className="pt-3">Create Rewards</h4>
                <img src="../../images/userdash.png" alt="User" />
                <Button
                  variant="contained"
                  color="default"
                  className="d-flex mt-3 mauto"
                >
                  <Link to="/">Choose File</Link>
                </Button>
              </div>
            </div>
          </div>
          <div className="createreward-section pt-5 ml-4 flex-grow-1">
            <form>
              <div className="form-group row required">
                <div className="col-md-6 d-flex">
                  <label htmlFor="name" className="col-sm-4 col-form-label">
                    First Name
                  </label>
                  <div className="col-sm-8">
                    <input
                      type="text"
                      className="form-control"
                      id="name"
                      required
                    />
                  </div>
                </div>
                <div className="col-md-6 d-flex">
                  <label htmlFor="name" className="col-sm-4 col-form-label">
                    Team
                  </label>
                  <div className="col-md-8">
                    <select id="Team" className="form-control">
                      <option value="31">amazon</option>
                      <option value="30">Facebook</option>
                      <option value="28">EBookCommerece</option>
                      <option value="24">ROBOTICS APP</option>
                      <option value="23">Automobile Application</option>
                      <option value="22">athena</option>
                      <option value="21">EShopping</option>
                      <option value="14">invoice</option>
                      <option value="13">Alexa</option>
                      <option value="12">Athena</option>
                      <option value="9">Shopcpr</option>
                      <option value="7">invoice</option>
                      <option value="4">invoice</option>
                      <option value="1">DEMO PROJECT</option>
                    </select>
                  </div>
                </div>
              </div>
              <div className="form-group row required">
                <div className="col-md-6 d-flex">
                  <label htmlFor="name" className="col-sm-4 col-form-label">
                    ID
                  </label>
                  <div className="col-sm-8">
                    <input
                      type="text"
                      className="form-control"
                      id="name"
                      required
                    />
                  </div>
                </div>
                <div className="col-md-6 d-flex">
                  <label htmlFor="name" className="col-sm-4 col-form-label">
                    Experience
                  </label>
                  <div className="col-md-8">
                    <select id="exp" className="form-control">
                      <option value="31">1</option>
                      <option value="30">2</option>
                      <option value="28">3</option>
                      <option value="24">4</option>
                      <option value="23">5</option>
                    </select>
                  </div>
                </div>
              </div>
              <div className="form-group row required">
                <div className="col-md-6 d-flex">
                  <label htmlFor="email" className="col-sm-4 col-form-label">
                    Email
                  </label>
                  <div className="col-sm-8">
                    <input
                      type="text"
                      className="form-control"
                      id="email"
                      required
                    />
                  </div>
                </div>
                <div className="col-md-6 d-flex">
                  <label htmlFor="name" className="col-sm-4 col-form-label">
                    Department
                  </label>
                  <div className="col-md-8">
                    <select id="status" className="form-control">
                      <option value="31">Heart Bangalore</option>
                      <option value="30">QCT</option>
                    </select>
                  </div>
                </div>
              </div>
              <div className="form-group row required">
                <div className="col-md-6 d-flex">
                  <label htmlFor="email" className="col-sm-4 col-form-label">
                    Status
                  </label>
                  <div className="col-sm-8">
                    <select id="status" className="form-control">
                      <option value="31">FullTime</option>
                      <option value="30">PartTime</option>
                    </select>
                  </div>
                </div>
                <div className="col-md-6 d-flex">
                  <label htmlFor="email" className="col-sm-4 col-form-label">
                    Position
                  </label>
                  <div className="col-sm-8">
                    <select id="status" className="form-control">
                      <option value="31">Graphic Designer</option>
                      <option value="30">Web Developer</option>
                    </select>
                  </div>
                </div>
              </div>
              <div className="form-group row required">
                <div className="col-md-6 d-flex">
                  <label htmlFor="email" className="col-sm-4 col-form-label">
                    Bio
                  </label>
                  <div className="col-sm-8">
                    <textarea
                      className="form-control"
                      id="textarea"
                      placeholder=" Highly creative and multitalented graphic designer"
                      rows={3}
                    />
                  </div>
                </div>
                <div className="col-md-6 d-flex" />
              </div>
              <div className="form-group row required">
                <div className="col-md-6 d-flex">
                  <label htmlFor="email" className="col-sm-4 col-form-label">
                    Projects
                  </label>
                  <div className="col-sm-8">
                    <div className="box">
                      <div className="d-flex justify-content-around">
                        <Button
                          variant="contained"
                          color="default"
                          className="d-flex mt-3 projbtn"
                        >
                          CCME
                        </Button>
                        <Button
                          variant="contained"
                          color="default"
                          className="d-flex mt-3 projbtn"
                        >
                          VoT
                        </Button>
                      </div>
                      <div className="d-flex justify-content-around">
                        <Button
                          variant="contained"
                          color="default"
                          className="d-flex mt-3 projbtn"
                        >
                          Invoice
                        </Button>
                        <Button
                          variant="contained"
                          color="default"
                          className="d-flex mt-3 projbtn"
                        >
                          VHF
                        </Button>
                      </div>
                    </div>
                  </div>
                </div>
                <div className="col-md-6 d-flex" />
              </div>
              <div className="form-group row required">
                <div className="col-md-6 d-flex">
                  <label htmlFor="email" className="col-sm-4 col-form-label">
                    Manager
                  </label>
                  <div className="col-sm-8">
                    <select id="status" className="form-control">
                      <option value="31">Lijoy George</option>
                      <option value="30">Roopa</option>
                    </select>
                  </div>
                </div>
                <div className="col-md-6 d-flex" />
              </div>
            </form>
            <div className="buttonstyle">
              <Button variant="contained" color="default">
                Submit
              </Button>
            </div>
          </div>
        </div>
      </div>
      {/* Modal for Create Reward */}
      <div
        className="modal fade show aui-modal"
        id="modalTemplate3"
        tabIndex={-1}
        aria-labelledby="modalTitle"
        aria-modal="true"
        role="dialog"
      >
        <div className="modal-dialog modal-dialog-centered modal-sm">
          <div className="modal-content">
            <div className="modal-header">
              <h5 className="h5" id="modalTitle">
                <img
                  src="../images/trophy.svg"
                  alt-="trophy"
                  className="px-2"
                />
                Create reward
              </h5>
              <button
                type="button"
                className="close"
                data-dismiss="modal"
                aria-label="Close"
              >
                <span aria-hidden="true" className="aha-icon-cross" />
              </button>
            </div>
            <div className="modal-body">
              <form>
                <div className="form-group row required">
                  <div className="col-md-12">
                    <select className="form-control" id="selectStd">
                      <option>Employee of the Month</option>
                      <option>Employee of the Month</option>
                      <option>Employee of the Month</option>
                      <option>Employee of the Month</option>
                    </select>
                  </div>
                </div>
              </form>
              <div className="pb-4">
                <h5 className="h5" id="modalTitle">
                  <img
                    src="../images/Icon feather-award.svg"
                    alt-="trophy"
                    className="px-2"
                  />
                  Select Badge
                </h5>
                <img
                  src="../images/Icon awesome-award.svg"
                  alt-="trophy"
                  className="px-2 awardimg"
                />
                <img
                  src="../images/Icon awesome-award.svg"
                  alt-="trophy"
                  className="px-2 awardimg"
                />
                <img
                  src="../images/Icon awesome-award.svg"
                  alt-="trophy"
                  className="px-2 awardimg"
                />
                <img
                  src="../images/Icon awesome-award.svg"
                  alt-="trophy"
                  className="px-2 awardimg"
                />
                <img
                  src="../images/Icon awesome-award.svg"
                  alt-="trophy"
                  className="px-2 awardimg"
                />
                <img
                  src="../images/Icon ionic-ios-trophy.svg"
                  alt-="trophy"
                  className="px-2 awardimg"
                />
                <img
                  src="../images/Icon ionic-ios-trophy.svg"
                  alt-="trophy"
                  className="px-2 awardimg"
                />
                <img
                  src="../images/Icon ionic-ios-trophy.svg"
                  alt-="trophy"
                  className="px-2 awardimg"
                />
              </div>
              <div>
                <h5 className="h5" id="modalTitle">
                  <img
                    src="../images/Icon awesome-gift.svg"
                    alt-="trophy"
                    className="px-2"
                  />
                  Appreciations
                </h5>
                <form>
                  <div className="form-group row required">
                    <div className="col-md-12">
                      <select className="form-control" id="selectStd">
                        <option>Giving Points</option>
                      </select>
                    </div>
                  </div>
                  <div className="form-group row required">
                    <div className="col-md-12">
                      <textarea
                        className="form-control"
                        id="textarea"
                        placeholder="This Award is given out to the top Employee on a monthly basis"
                        rows={4}
                      />
                    </div>
                  </div>
                </form>
              </div>
            </div>
            <div className="modal-footer">
              <div className="col-md-12 offset-md-5">
                <button
                  type="button"
                  className="btn btn-primary btn-round btn-block"
                >
                  Send Rewards
                </button>
              </div>
            </div>
          </div>
        </div>
      </div>
    </CreateRewardWrapper>
  );
};

export default CreateReward;
